function errorHandler(err, req, res, next) {
  /*
  Determine the status code based on the error or default to 500 (Internal Server Error)
  Determine the error message based on the error or default to a generic message
  Respond to the client with an error message and status code
  */
  const statusCode = err.statusCode || 500;
  const errorMessage = err.message || "Internal Server Error";
  res.status(statusCode).json({ error: errorMessage });
}

module.exports = errorHandler;
