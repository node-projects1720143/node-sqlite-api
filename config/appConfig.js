module.exports = {
  port: process.env.PORT || 3000,
  databaseFileName: "data/database.db",
};
