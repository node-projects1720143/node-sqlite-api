const chai = require("chai");
const expect = chai.expect;
const categoryController = require("../../../app/controllers/categoryController");
const utils = require("../../../utils/utility");

describe("Category Controller (Integration Tests)", () => {
  let db;

  before(() => {
    db = utils.getTestDbConnection();
  });

  after(() => {
    db.close();
  });

  it("should return a nested category tree with query param", async () => {
    const req = {
      query: { id: 1 },
    };
    const res = {
      json: (data) => {
        expect(data).to.be.an("array");
        expect(data[0].id).to.equal(1);
        expect(data[0].name).to.equal("Category A");
      },
    };

    await categoryController.getCategoryTreeWithQuery(req, res, () => {});
  });

  it("should return a nested category tree with route param", async () => {
    const req = {
      params: { id: 1 },
    };
    const res = {
      json: (data) => {
        expect(data).to.be.an("array");
        expect(data[0].id).to.equal(1);
        expect(data[0].name).to.equal("Category A");
      },
    };

    await categoryController.getCategoryTreeWithParam(req, res, () => {});
  });
});
