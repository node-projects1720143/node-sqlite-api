/*
Create e2e tests for the following routes:
GET /categories
GET /categories?id=1
GET /categories/:id
*/
const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const app = require("../../app");

chai.use(chaiHttp);

describe("Category Routes (e2e)", () => {
  it("should return a nested category tree for GET /categories", async () => {
    const res = await chai.request(app).get("/api/categories");
    expect(res).to.have.status(200);
    expect(res.body).to.be.an("array");
  });

  it("should return a nested category tree for GET /api/categories?id=1", async () => {
    const res = await chai.request(app).get("/api/categories?id=1");
    expect(res).to.have.status(200);
    expect(res.body).to.be.an("array");
  });

  it("should return a nested category tree for GET /api/categories/:id", async () => {
    const res = await chai.request(app).get("/api/categories/1");
    expect(res).to.have.status(200);
    expect(res.body).to.be.an("array");
  });
});
