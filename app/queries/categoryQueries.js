const categoryQueries = {
  getFullCategoryTree: `
      SELECT * FROM categories;
    `,
  // This query uses a recursive common table expression (CTE) to get the
  // nested category tree. The CTE is named category_tree and it is used to
  // recursively select all the child categories of the given category id.
  // The CTE is then joined with the categories table to get the category
  // details for each category id.
  getCategoryTreeWithRoot: `
      WITH RECURSIVE category_tree AS (
          SELECT id, name, parent_id
          FROM categories
          WHERE id = ?
          UNION ALL
          SELECT c.id, c.name, c.parent_id
          FROM categories c
          JOIN category_tree ct ON c.parent_id = ct.id
      )
      SELECT * FROM category_tree;
    `,
};

module.exports = categoryQueries;
