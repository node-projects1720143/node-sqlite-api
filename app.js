const express = require("express");
const app = express();
const appConfig = require("./config/appConfig");
const errorHandler = require("./app/middlewares/errorHandler");
const categoryRoutes = require("./app/routes/categoryRoutes");

const port = appConfig.port;

app.use(express.json());

app.use("/api/", categoryRoutes);

// Using the error handler middleware
app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

module.exports = app;
