const sqlite3 = require("sqlite3").verbose();

// Create a nested category tree from flat category data
function createNestedCategoryTree(records, rootCategoryId = null) {
  const categoryTree = [];
  records.forEach((record) => {
    const { id, name, parent_id } = record;
    if (id == rootCategoryId || parent_id === null) {
      categoryTree.push({ id, name, parent_id, subcategories: [] });
    } else {
      const parentNode = findNodeById(categoryTree, parent_id);
      if (parentNode) {
        parentNode.subcategories.push({
          id,
          name,
          parent_id,
          subcategories: [],
        });
      }
    }
  });
  return categoryTree;
}

// Function to find a node in the category tree by its id
function findNodeById(categoryTree, id) {
  for (let i = 0; i < categoryTree.length; i++) {
    if (categoryTree[i].id === id) {
      return categoryTree[i];
    }
    const childNode = findNodeById(categoryTree[i].subcategories, id);
    if (childNode) {
      return childNode;
    }
  }
  return null;
}

function getTestDbConnection() {
  const db = new sqlite3.Database(":memory:");
  db.serialize(() => {
    db.run("CREATE TABLE categories (id INT, name TEXT, parent_id INT)");
    db.run(
      'INSERT INTO categories (id, name, parent_id) VALUES (1, "Category A", null)'
    );
    db.run(
      'INSERT INTO categories (id, name, parent_id) VALUES (2, "Category B", null)'
    );
    db.run(
      'INSERT INTO categories (id, name, parent_id) VALUES (3, "Subcategory A", 1)'
    );
  });
  return db;
}

module.exports = {
  createNestedCategoryTree,
  findNodeById,
  getTestDbConnection,
};
