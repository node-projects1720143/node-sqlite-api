-- schema.sql

-- Create the 'categories' table
CREATE TABLE IF NOT EXISTS categories (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  parent_id INTEGER,  -- References the id of the parent category
  FOREIGN KEY (parent_id) REFERENCES categories(id)
);


-- Top-level categories
INSERT INTO categories (name, parent_id) VALUES ('Electronics', null);
INSERT INTO categories (name, parent_id) VALUES ('Clothing', null);
INSERT INTO categories (name, parent_id) VALUES ('Home & Garden', null);

-- Subcategories
INSERT INTO categories (name, parent_id) VALUES ('Computers', 1);  -- Electronics
INSERT INTO categories (name, parent_id) VALUES ('Laptops', 4);    -- Computers
INSERT INTO categories (name, parent_id) VALUES ('Desktops', 4);   -- Computers
INSERT INTO categories (name, parent_id) VALUES ('Kitchen', 3);    -- Home & Garden
INSERT INTO categories (name, parent_id) VALUES ('Furniture', 3);  -- Home & Garden

-- Additional categories
INSERT INTO categories (name, parent_id) VALUES ('Men''s Clothing', 2);    -- Clothing
INSERT INTO categories (name, parent_id) VALUES ('Women''s Clothing', 2);  -- Clothing
INSERT INTO categories (name, parent_id) VALUES ('Bedroom Furniture', 8); -- Furniture
INSERT INTO categories (name, parent_id) VALUES ('Dining Room Furniture', 8); -- Furniture
INSERT INTO categories (name, parent_id) VALUES ('Smartphones', 1);  -- Electronics
INSERT INTO categories (name, parent_id) VALUES ('Tablets', 1);      -- Electronics
INSERT INTO categories (name, parent_id) VALUES ('Casual Wear', 11);  -- Men's Clothing
INSERT INTO categories (name, parent_id) VALUES ('Formal Wear', 11);  -- Men's Clothing
INSERT INTO categories (name, parent_id) VALUES ('Dresses', 12);     -- Women's Clothing
INSERT INTO categories (name, parent_id) VALUES ('Footwear', 12);    -- Women's Clothing
