const chai = require("chai");
const expect = chai.expect;
const CategoryModel = require("../../../app/models/categoryModel");
const utils = require("../../../utils/utility");

describe("Category Model", () => {
  let model;
  let db;

  before(() => {
    // Create a new in-memory database and initialize the model
    db = utils.getTestDbConnection();
    model = new CategoryModel(db);
  });

  after(() => {
    // Close the in-memory database after all tests are done
    db.close();
  });

  it("should retrieve the full category tree", async () => {
    const result = await model.getFullCategoryTree();
    expect(result).to.be.an("array");
    expect(result).to.have.lengthOf.at.least(3); // there are 3 items in the test database
  });

  it("should retrieve a specific category tree", async () => {
    const result = await model.getCategoryTreeWithRoot(1); // category ID is 1
    expect(result).to.be.an("array");
    expect(result).to.have.lengthOf.at.least(2);
  });
});
