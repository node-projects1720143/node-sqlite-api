# NodeJS SQLITE API

This is a Node.js API that uses SQLite as a database. The API is built with Express.js and Docker is used for deployment.

### Prerequisites

- Node.js
- SQLite
- NPM
- Docker (for deployment)

## Tests

Run the following command to run tests to collect all the tests in the `tests` directory and subdirectories.

    ```bash
    npm run test
    ```

## API Documentation

### Get all categories

Returns all categories in a nested format.

- **URL**

    /api/categories?id=:id

- **Method**
    
    `GET`

- **URL Params**

    **Optional:**
    
    `id=[integer]`

- **Success Response:**

    - **Code:** 200 <br />
    **Content:** 
    ```json
    [
        {
            "id": 1,
            "name": "Category 1",
            "parent_id": null,
            "subcategories": [
                {
                    "id": 2,
                    "name": "Category 1.1",
                    "parent_id": 1,
                    "subcategories": [
                        {
                            "id": 3,
                            "name": "Category 1.1.1",
                            "parent_id": 2,
                            "subcategories": []
                        }
                    ]
                }
            ]
        }
    ]
    ```

- **Error Response:**

    - **Code:** 404 NOT FOUND <br />
    **Content:** `{"error": "Category ID XX does not exist!"}`

    - **Code:** 500 INTERNAL SERVER ERROR <br />
    **Content:** `{"error": "Internal server error!"}`



## Deployment with Docker

- Build the image

    ```bash
    docker build -t nodejs-sqlite-api .
    ```
- Run the container

    ```bash
    docker run -p 3000:3000 -d nodejs-sqlite-api
    ```
- Access the API at http://localhost:3000

## Built With

- [Node.js](https://nodejs.org/en/) - JavaScript runtime
- [Express.js](https://expressjs.com/) - Web framework
- [SQLite](https://www.sqlite.org/index.html) - Database
- [Docker](https://www.docker.com/) - Containerization platform
- [Mocha](https://mochajs.org/) - Testing framework
- [Chai](https://www.chaijs.com/) - Assertion library
