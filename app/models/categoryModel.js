/**
 * Represents the Category model for interacting with category data in the database.
 *
 * @class Category
 * @since 2023-10-25
 */
const sqlite3 = require("sqlite3").verbose();
const appConfig = require("../../config/appConfig");
const categoryQueries = require("../queries/categoryQueries");
const logger = require("../../config/logConfig");

class Category {
  constructor() {
    this.db = new sqlite3.Database(appConfig.databaseFileName);
  }

  /**
   * Retrieves the full category tree from the database.
   * @async
   * @returns {Promise<Array>} An array of category tree data.
   * @throws {Error} If there's an error in retrieving the category tree.
   */
  async getFullCategoryTree() {
    try {
      const rows = await new Promise((resolve, reject) => {
        this.db.all(categoryQueries.getFullCategoryTree, (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        });
      });
      return rows;
    } catch (error) {
      logger.error("Error in getFullCategoryTree:", error);
      throw error;
    }
  }

  /**
   * Retrieves a category tree with a specified root ID from the database.
   * @async
   * @param {number} id - The ID of the root category.
   * @returns {Promise<Array>} An array of category tree data.
   * @throws {Error} If there's an error in retrieving the category tree.
   */
  async getCategoryTreeWithRoot(id) {
    try {
      const rows = await new Promise((resolve, reject) => {
        this.db.all(
          categoryQueries.getCategoryTreeWithRoot,
          [id],
          (err, rows) => {
            if (err) {
              reject(err);
            } else {
              resolve(rows);
            }
          }
        );
      });
      return rows;
    } catch (error) {
      logger.error("Error in getCategoryTreeWithRoot:", error);
      throw error;
    }
  }
}

module.exports = Category;
