/**
 * CategoryService class provides methods to retrieve category tree data.
 * @class CategoryService
 */

const Category = require("../models/categoryModel");
const categoryTreeUtils = require("../../utils/utility");

class CategoryService {
  constructor() {
    this.categoryModel = new Category();
  }

  /**
   * Retrieves the full category tree.
   * @async
   * @returns {Promise<Array>} A nested category tree.
   */
  async getFullCategoryTree() {
    const rows = await this.categoryModel.getFullCategoryTree();
    return categoryTreeUtils.createNestedCategoryTree(rows);
  }

  /**
   * Retrieves a category tree with a specified root ID.
   * @async
   * @param {number} [id] - The ID of the root category.
   * @returns {Promise<Array>} A nested category tree with the specified root.
   */
  async getCategoryTree(id = null) {
    if (!id) {
      return this.getFullCategoryTree();
    }
    const rows = await this.categoryModel.getCategoryTreeWithRoot(id);
    return categoryTreeUtils.createNestedCategoryTree(rows, id);
  }
}

module.exports = CategoryService;
