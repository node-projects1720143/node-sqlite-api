const chai = require("chai");
const expect = chai.expect;
const sinon = require("sinon");
const CategoryService = require("../../../app/services/categoryService");
const CategoryModel = require("../../../app/models/categoryModel");

describe("Category Service", () => {
  let categoryService;
  let categoryModelStub;

  before(() => {
    categoryService = new CategoryService();
  });

  beforeEach(() => {
    categoryModelStub = sinon.createStubInstance(CategoryModel);
  });

  it("should return a nested category tree for getFullCategoryTree", async () => {
    const mockData = [
      { id: 1, name: "Category A", parent_id: null },
      { id: 2, name: "Category B", parent_id: null },
    ];
    const expectedNestedTree = [
      {
        id: 1,
        name: "Category A",
        parent_id: null,
        subcategories: [],
      },
      {
        id: 2,
        name: "Category B",
        parent_id: null,
        subcategories: [],
      },
    ];

    // Stub the getFullCategoryTree method in the categoryModel instance
    categoryModelStub.getFullCategoryTree.returns(Promise.resolve(mockData));

    // Replace the categoryModel instance in the service with the stub
    categoryService.categoryModel = categoryModelStub;

    const nestedCategoryTree = await categoryService.getFullCategoryTree();

    expect(nestedCategoryTree).to.deep.equal(expectedNestedTree);
  });

  it("should return a nested category tree for getCategoryTree", async () => {
    const categoryId = 1;
    const mockData = [
      { id: 1, name: "Category A", parent_id: null },
      { id: 3, name: "Subcategory A", parent_id: 1 },
    ];
    const expectedNestedTree = [
      {
        id: 1,
        name: "Category A",
        parent_id: null,
        subcategories: [
          {
            id: 3,
            name: "Subcategory A",
            parent_id: 1,
            subcategories: [],
          },
        ],
      },
    ];

    // Stub the getCategoryTreeWithRoot method in the categoryModel instance
    categoryModelStub.getCategoryTreeWithRoot
      .withArgs(categoryId)
      .returns(Promise.resolve(mockData));

    // Replace the categoryModel instance in the service with the stub
    categoryService.categoryModel = categoryModelStub;

    const nestedCategoryTree = await categoryService.getCategoryTree(
      categoryId
    );

    expect(nestedCategoryTree).to.deep.equal(expectedNestedTree);
  });
});
