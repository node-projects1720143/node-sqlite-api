const logger = require("../../config/logConfig");
const CategoryService = require("../services/categoryService");
const categoryService = new CategoryService();

/**
 * Get a nested category tree based on a query parameter.
 * @param {Request} req - Express Request object.
 * @param {Response} res - Express Response object.
 * @param {Function} next - Express Next function.
 */
async function getCategoryTreeWithQuery(req, res, next) {
  const id = req.query.id || null;
  try {
    // Retrieve a nested category tree using the CategoryService
    const nestedCategoryTree = await categoryService.getCategoryTree(id);

    // Check if the nestedCategoryTree is empty; if so, the category ID does not exist
    if (nestedCategoryTree.length > 0) {
      res.status(200).json(nestedCategoryTree);
    } else {
      logger.info("Category not found with Category ID - " + id);
      res.status(404).json({ error: "Category ID " + id + " does not exist!" });
    }
  } catch (error) {
    // Catch any errors from the service layer and pass them to the error handler middleware
    next(error);
  }
}

/**
 * Get a nested category tree based on a route parameter.
 * @param {Request} req - Express Request object.
 * @param {Response} res - Express Response object.
 * @param {Function} next - Express Next function.
 */
async function getCategoryTreeWithParam(req, res, next) {
  const id = req.params.id;
  try {
    // Retrieve a nested category tree using the CategoryService
    const nestedCategoryTree = await categoryService.getCategoryTree(id);

    // Check if the nestedCategoryTree is empty; if so, the category ID does not exist
    if (nestedCategoryTree.length > 0) {
      res.status(200).json(nestedCategoryTree);
    } else {
      logger.info("Category not found with Category ID - " + id);
      res.status(404).json({ error: "Category ID " + id + " does not exist!" });
    }
  } catch (error) {
    // Catch any errors from the service layer and pass them to the error handler middleware
    next(error);
  }
}

module.exports = {
  getCategoryTreeWithQuery,
  getCategoryTreeWithParam,
};
