// routes/categoryRoutes.js
const express = require("express");
const router = express.Router();
const categoryController = require("../controllers/categoryController");

router.get("/categories", categoryController.getCategoryTreeWithQuery);
// example of extending the route
router.get("/categories/:id", categoryController.getCategoryTreeWithParam);

module.exports = router;
